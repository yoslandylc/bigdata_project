#!/bin/bash

# run N slave containers

if [ $# != 2  ]
then
	echo "Intoduzca los parámetros de replicación: (F) Filas (C) Columnas
			Ej: 3 4, donde 3 es la cantidad de filas y 4 es la cantidad de columnas"
	exit 1
fi

python insert_to_hbase.py $1 $2
