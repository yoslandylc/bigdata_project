#!/usr/bin/python3
import csv
import os
import sys
import hashlib
import happybase


def action_insert_to_hbase(f, c):
    csv_file = open('%s/dataset/SET-dec-2013.csv' % str(os.getcwd()), 'rb')
    rows = csv.reader(csv_file, delimiter=',', quotechar='|')
    families = {'SensorInfo': {}}
    for cont in range(c):
        families.update({"Measure%s" % str(cont+1): {}})
    connector = happybase.Connection(host="master.krejcmat.com", port=9090)
    connector.open()
    print("Se estan creando las familias %s" % families.keys())
    tables = connector.tables()
    if "Sensores" in tables:
        table = connector.tables('Sensores')
    else:
        connector.create_table('Sensores', families)
        table = connector.table('Sensores')
    print("Creadas las familias %s" % families.keys())
    batch = table.batch(batch_size=1000)
    for row in rows:
        for colf in range(f):
            row_key = hashlib.md5(row[0] + row[1].split(' ')[0]).hexdigest()
            SensorInfo = {'SensorInfo:Sensor': str(colf+1) + row[0],
                      'SensorInfo:Date': row[1].split(' ')[0]}
            for contc in range(c):
                SensorInfo.update({"Measure%d:%s" % (contc+1, row[1].split(' ')[1]): row[2]})
            batch.put(row_key, SensorInfo)
            print("Insert into hbase row_key:%s and values:%s" % (row_key, str(SensorInfo)))
            batch.send()
    print("Finalizado")
    connector.close()


if __name__ == "__main__":
    arg = sys.argv
    try:
        f = int(arg[1])
        c = int(arg[2])
        print("Se crearan %s filas y %s columnas" % (f,c))
    except:
        print("Error: Solo se permiten la entrada de numeros enteros")
    try:
        action_insert_to_hbase(f, c)
    except Exception as e:
        print("Error: Un error ha ocurrido durante el proceso. " +str(e))
