#!/usr/bin/python3
import csv
import happybase
import os
import sys
from datetime import datetime
from dateutil.relativedelta import relativedelta


def action_result_from_hbase(f, c):
    with open('%s/result.csv' % str(os.getcwd()), 'wb') as csvfile:
        connector = happybase.Connection(host="master.krejcmat.com", port=9090)
        connector.open()
        table = connector.table('Sensores')
        print("Conexion a hbase realizada")
        hours = ["00:00"]
        current_date = datetime.strptime(str(datetime.today().date()) + " 00:00", "%Y-%m-%d %H:%M")
        for r in range(143):
            current_date += relativedelta(minutes=+10)
            hours.append(current_date.time().strftime("%H:%M"))
        writer = csv.DictWriter(csvfile, fieldnames=["Sensor", "Date"]+hours)
        writer.writeheader()
        print("Escribiendo las cabeceras en el csv")
        filter = b"SingleColumnValueFilter ('SensorInfo', 'Sensor',=, 'regexstring:^"+str(f)+"DG')"
        for key, values in table.scan(filter=filter, columns=['SensorInfo', 'Measure'+str(c)], sorted_columns=True):
            write_dict = {'Sensor': values.get("SensorInfo:Sensor"),
                          'Date': values.get("SensorInfo:Date")}
            for hour in hours:
                write_dict.update({hour: values.get("%s:%s" % ("Measure"+str(c), hour))})
            print("Escribiendo los valores:%s en el csv" % str(write_dict))
            writer.writerow(write_dict)
        connector.close()


if __name__ == "__main__":
    arg = sys.argv
    try:
        f = int(arg[1])
        c = int(arg[2])
        print("Se obtendran el medidor %s de la columna %s" % (f, c))
    except:
        print("Error: Solo se permiten la entrada de numeros enteros")
    try:
        action_result_from_hbase(f, c)
    except:
        print("Error: Un error ha ocurrido durante el proceso.")