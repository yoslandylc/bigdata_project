#!/bin/bash

# run N slave containers

if [ $# != 2  ]
then
	echo "Debe introducir los dos parametro para la extraccion, el primero (F) seran las filas, el segundo (C) las columnas"
	exit 1
fi

python return_from_hbase.py $1 $2
